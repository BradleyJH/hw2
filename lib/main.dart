import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is my Shih Tzu' , style:TextStyle(
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('Hes the best doggo ever!', style: TextStyle(
                  color:Colors.grey[500],),
                ),
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.blue[500],),
          Text('11'),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;


    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.ac_unit, 'Chill'),
          _buildButtonColumn(color, Icons.access_time, 'Wait'),
          _buildButtonColumn(color, Icons.local_pizza, 'Food'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my dog named Koda (Disneys Brother Bear reference).'
        'He is a pure bred Shih Tzu that we had purchased from a breeder.'
        'He is very well trained. He is trained to sit, lay down and '
        'chill(our version of stay). With Chill we can throw a treat and as '
        'long as we say chill he wont go after his treat until we nod '
        'telling him to go get it. He is also dating my families Shih Tzu coco.',
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Layout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'Images/koda.jpeg',
              width: 600,
              height: 480,
              fit: BoxFit.cover,
            ),
            titleSection,
            buttonSection,
            textSection
          ]
        ),
      ),
    );
  }
  Column _buildButtonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color:color,
            ),
          ),
        ),
      ],
    );
  }
}

